package searchmachinepackage;

import java.util.ArrayList;
import java.util.Scanner;
import machinepackge.*;

public class SearchMachine {
    public static void searchMachine(ArrayList<Machine> machines) {
        char choiceToSearch = '\0';
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter your choice\n");
        System.out.println("=========================================");
        System.out.println("A: Search machine by name");
        System.out.println("B: Search machine by purchase year");
        System.out.println("C: Search machine by current status");
        System.out.println("D: Exit");
        System.out.println("=========================================");
        choiceToSearch = sc.next().charAt(0);

        switch (choiceToSearch) {
            case 'A':
                searchByName(machines);
                break;
            case 'B':
                searchByPurchaseYear(machines);

                break;
            case 'C':
                searchByCurrentStatus(machines);

                break;
            case 'D':

                break;
            default:
                System.out.println("Invalid choice!");
                break;
        }
    }

    private static void searchByCurrentStatus(ArrayList<Machine> machines) {
        char choice = '\0';
        Scanner sc = new Scanner(System.in);
        System.out.println("Select current status");
        System.out.println("=========================================");
        System.out.println("A: Functional");
        System.out.println("B: Non-Functional");
        System.out.println("C: Need Maintainance");
        System.out.println("D: Exit");
        System.out.println("=========================================");
        choice = sc.next().charAt(0);

        switch (choice) {
            case 'A':
                searchFunctionalMachine(machines);
                break;
            case 'B':
                searchNonFunctionalMachine(machines);

                break;
            case 'C':
                searchNeedMaintainanceMachine(machines);

                break;
            case 'D':

                break;
            default:
                System.out.println("Invalid choice! Please select valid option");
                break;
        }
    }

    private static void searchNeedMaintainanceMachine(ArrayList<Machine> machines) {
        String searchStatus = "Need Maintainance";
        for (int counter = 0; counter < machines.size(); counter++) {

            if (searchStatus.equals(machines.get(counter).getCurrentStatus())) {
                System.out.println("=========================================");
                System.out.println("Machine id is " + machines.get(counter).getId());
                System.out.println("Machine name is " + machines.get(counter).getName());
                System.out.println("Purchase year is " + machines.get(counter).getPurchaseYear());
                System.out.println("Engine capacity is " + machines.get(counter).getEngineCapacity());
                System.out.println("Current status is " + machines.get(counter).getCurrentStatus());
                System.out.println("=========================================");
            }
        }
    }

    private static void searchNonFunctionalMachine(ArrayList<Machine> machines) {
        String searchStatus = "Non-Functional";
        for (int counter = 0; counter < machines.size(); counter++) {

            if (searchStatus.equals(machines.get(counter).getCurrentStatus())) {
                System.out.println("=========================================");
                System.out.println("Machine id is " + machines.get(counter).getId());
                System.out.println("Machine name is " + machines.get(counter).getName());
                System.out.println("Purchase year is " + machines.get(counter).getPurchaseYear());
                System.out.println("Engine capacity is " + machines.get(counter).getEngineCapacity());
                System.out.println("Current status is " + machines.get(counter).getCurrentStatus());
                System.out.println("=========================================");
            }
        }
    }

    private static void searchFunctionalMachine(ArrayList<Machine> machines) {
        String searchStatus = "Functional";
        for (int counter = 0; counter < machines.size(); counter++) {

            if (searchStatus.equals(machines.get(counter).getCurrentStatus())) {
                System.out.println("=========================================");
                System.out.println("Machine id is " + machines.get(counter).getId());
                System.out.println("Machine name is " + machines.get(counter).getName());
                System.out.println("Purchase year is " + machines.get(counter).getPurchaseYear());
                System.out.println("Engine capacity is " + machines.get(counter).getEngineCapacity());
                System.out.println("Current status is " + machines.get(counter).getCurrentStatus());
                System.out.println("=========================================");
            }
        }
    }

    private static void searchByPurchaseYear(ArrayList<Machine> machines) {
        int searchPurchaseYear = 0;
        boolean isNumber = false;
        Scanner sc = new Scanner(System.in);

        do {
            try {
                System.out.println("Enter purchase year\n");
                searchPurchaseYear = sc.nextInt();
                isNumber = true;
            } catch (Exception e) {
                System.out.println("=========================================");
                System.out.println("Enter valid year");
                System.out.println("=========================================");
                isNumber = false;
                sc.next();
                continue;
            }
        } while (!isNumber);
        sc.nextLine();
        System.out.println();

        for (int counter = 0; counter < machines.size(); counter++) {

            if (searchPurchaseYear == machines.get(counter).getPurchaseYear()) {
                System.out.println("=========================================");
                System.out.println("Machine id is " + machines.get(counter).getId());
                System.out.println("Machine name is " + machines.get(counter).getName());
                System.out.println("Purchase year is " + machines.get(counter).getPurchaseYear());
                System.out.println("Engine capacity is " + machines.get(counter).getEngineCapacity());
                System.out.println("Current status is " + machines.get(counter).getCurrentStatus());
                System.out.println("=========================================");
            }
        }
    }

    private static void searchByName(ArrayList<Machine> machines) {
        String searchName;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter machine name\n");
        searchName = sc.nextLine();

        for (int counter = 0; counter < machines.size(); counter++) {

            if (searchName.equals(machines.get(counter).getName())) {
                System.out.println("=========================================");
                System.out.println("Machine id is " + machines.get(counter).getId());
                System.out.println("Machine name is " + machines.get(counter).getName());
                System.out.println("Purchase year is " + machines.get(counter).getPurchaseYear());
                System.out.println("Engine capacity is " + machines.get(counter).getEngineCapacity());
                System.out.println("Current status is " + machines.get(counter).getCurrentStatus());
                System.out.println("=========================================");
            }
        }
    }
}
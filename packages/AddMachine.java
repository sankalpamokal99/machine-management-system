package addmachinepackage;

import java.util.ArrayList;
import java.util.Scanner;
import machinepackge.*;

public class AddMachine {
    public static void addMachine(ArrayList<Machine> machines) {
        boolean isNumber = false;
        Scanner sc = new Scanner(System.in);
        char choiceCurrentStatus = '\0';
        int id = 0;
        String name;
        int purchaseYear = 0;
        int engineCapacity = 0;
        String currentStatus = "";

        do {
            try {
                System.out.println("Enter machine id\n");
                id = sc.nextInt();
                isNumber = true;
            } catch (Exception e) {
                System.out.println("=========================================");
                System.out.println("Enter valid integer id");
                System.out.println("=========================================");
                isNumber = false;
                sc.next();
                continue;
            }
        } while (!isNumber);
        sc.nextLine();
        System.out.println();

        System.out.println("Enter machine name\n");
        name = sc.nextLine();
        System.out.println();

        do {
            try {
                System.out.println("Enter purchase year\n");
                purchaseYear = sc.nextInt();
                isNumber = true;
            } catch (Exception e) {
                System.out.println("=========================================");
                System.out.println("Enter valid year");
                System.out.println("=========================================");
                isNumber = false;
                sc.next();
                continue;
            }
        } while (!isNumber);
        sc.nextLine();
        System.out.println();

        do {
            try {
                System.out.println("Enter engine capacity\n");
                engineCapacity = sc.nextInt();
                isNumber = true;
            } catch (Exception e) {
                System.out.println("=========================================");
                System.out.println("Enter valid engine capacity");
                System.out.println("=========================================");
                isNumber = false;
                sc.next();
                continue;
            }
        } while (!isNumber);
        sc.nextLine();
        System.out.println();

        System.out.println("Enter current status\n");
        System.out.println("=========================================");
        System.out.println("A: Functional");
        System.out.println("B: Non-Functional");
        System.out.println("C: Need Maintainance");
        System.out.println("D: Exit");
        System.out.println("=========================================");
        choiceCurrentStatus = sc.next().charAt(0);

        switch (choiceCurrentStatus) {
            case 'A':
                currentStatus = "Functional";
                break;
            case 'B':
                currentStatus = "Non-Functional";

                break;
            case 'C':
                currentStatus = "Need Maintainance";

                break;
            case 'D':

                break;
            default:
                System.out.println("Invalid choice!");
                break;
        }
        if (choiceCurrentStatus == 'A' || choiceCurrentStatus == 'B' || choiceCurrentStatus == 'C') {
            Machine m = new Machine(id, name, purchaseYear, engineCapacity, currentStatus);

            machines.add(m);
            System.out.println("Machine added\n");
            System.out.println("=========================================");
            System.out.println("Machine id is " + m.getId());
            System.out.println("Machine name is " + m.getName());
            System.out.println("Machine's purchase year is " + m.getPurchaseYear());
            System.out.println("Machine's current status is " + m.getCurrentStatus());
            System.out.println("Machine's engine capacity is " + m.getEngineCapacity());
            System.out.println("=========================================");
        } else if (choiceCurrentStatus == 'D') {
            System.out.println("=========================================");
            System.out.println("Machine not added\n");
            System.out.println("=========================================");
        } else {
            System.out.println("=========================================");
            System.out.println("Enter valid option for current status\n");
            System.out.println("=========================================");
        }
    }
}
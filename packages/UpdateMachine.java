package updatemachinepackage;

import java.util.ArrayList;
import java.util.Scanner;
import machinepackge.*;

public class UpdateMachine {
    public static void updateMachine(ArrayList<Machine> machines) {
        boolean isNumber = false;
        char choiceToUpdate = '\0';
        int machineId = 0;
        Scanner sc = new Scanner(System.in);

        do {
            try {
                System.out.println("Enter machine id\n");
                machineId = sc.nextInt();
                isNumber = true;
            } catch (Exception e) {
                System.out.println("=========================================");
                System.out.println("Enter valid integer id");
                System.out.println("=========================================");
                isNumber = false;
                sc.next();
                continue;
            }
        } while (!isNumber);
        sc.nextLine();
        System.out.println();

        System.out.println("Enter your choice\n");
        System.out.println("=========================================");
        System.out.println("A: Update machine name");
        System.out.println("B: Update engine capacity");
        System.out.println("C: Update current status");
        System.out.println("D: Exit");
        System.out.println("=========================================");
        choiceToUpdate = sc.next().charAt(0);

        switch (choiceToUpdate) {
            case 'A':
                updateMachineName(machineId, machines);
                break;
            case 'B':
                updateEngineCapacity(machineId, machines);

                break;
            case 'C':
                UpdateCurrentStatus(machineId, machines);

                break;
            case 'D':

                break;
            default:
                System.out.println("Invalid choice!");
                break;
        }
    }

    private static void UpdateCurrentStatus(int machineId, ArrayList<Machine> machines) {
        String newCurrentStatus = "";
        char choiceNewCurrentStatus = '\0';
        Scanner sc = new Scanner(System.in);

        System.out.println("Select new status\n");
        System.out.println("=========================================");
        System.out.println("A: Functional");
        System.out.println("B: Non-Functional");
        System.out.println("C: Need Maintainance");
        System.out.println("D: Exit");
        System.out.println("=========================================");
        choiceNewCurrentStatus = sc.next().charAt(0);

        switch (choiceNewCurrentStatus) {
            case 'A':
                newCurrentStatus = "Functional";
                break;
            case 'B':
                newCurrentStatus = "Non Functional";

                break;
            case 'C':
                newCurrentStatus = "Need Maintainance";

                break;
            case 'D':

                break;
            default:
                System.out.println("Invalid choice!");
                break;
        }
        if (choiceNewCurrentStatus == 'A' || choiceNewCurrentStatus == 'B' || choiceNewCurrentStatus == 'C') {
            for (int counter = 0; counter < machines.size(); counter++) {

                if (machineId == machines.get(counter).getId()) {
                    machines.get(counter).setCurrentStatus(newCurrentStatus);
                    System.out.println("=========================================");
                    System.out.println("Current Status updated");
                    System.out.println("=========================================");
                }
            }
        } else if (choiceNewCurrentStatus == 'D') {
            System.out.println("=========================================");
            System.out.println("Updation Cancelled");
            System.out.println("=========================================");
        } else {
            System.out.println("=========================================");
            System.out.println("Please select valid option");
            System.out.println("=========================================");
        }
    }

    private static void updateEngineCapacity(int machineId, ArrayList<Machine> machines) {
        boolean isNumber = false;
        int newEngineCapacity = 0;
        Scanner sc = new Scanner(System.in);

        do {
            try {
                System.out.println("Enter new engine capacity to update\n");
                newEngineCapacity = sc.nextInt();
                isNumber = true;
            } catch (Exception e) {
                System.out.println("=========================================");
                System.out.println("Enter valid engine capacity");
                System.out.println("=========================================");
                isNumber = false;
                sc.next();
                continue;
            }
        } while (!isNumber);
        sc.nextLine();
        System.out.println();

        for (int counter = 0; counter < machines.size(); counter++) {

            if (machineId == machines.get(counter).getId()) {
                machines.get(counter).setEngineCapacity(newEngineCapacity);
                System.out.println("=========================================");
                System.out.println("Engine capacity updated");
                System.out.println("=========================================");
            }
        }
    }

    private static void updateMachineName(int machineId, ArrayList<Machine> machines) {
        String newMachineName;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter new name to update\n");
        newMachineName = sc.nextLine();

        for (int counter = 0; counter < machines.size(); counter++) {

            if (machineId == machines.get(counter).getId()) {
                machines.get(counter).setName(newMachineName);
                System.out.println("=========================================");
                System.out.println("Machine name updated");
                System.out.println("=========================================");

            }
        }
    }
}
package listmachinepackage;

import java.util.ArrayList;
import machinepackge.*;

public class ListMachine {
    public static void listAllMachines(ArrayList<Machine> machines) {

        machines.forEach(machine -> {
            System.out.println("=========================================");
            System.out.println("Machine id is " + machine.getId());
            System.out.println("Machine name is " + machine.getName());
            System.out.println("Purchase year is " + machine.getPurchaseYear());
            System.out.println("Engine capacity is " + machine.getEngineCapacity());
            System.out.println("Current status is " + machine.getCurrentStatus());
            System.out.println("=========================================");
        });

    }
}
package packages;

import java.util.ArrayList;
import java.util.Scanner;

public class RemoveMachine {
    public static void removeMachine(ArrayList<Machine> machines) {
        String machineName;
        System.out.println("Enter machine name\n");
        Scanner sc = new Scanner(System.in);

        machineName = sc.nextLine();
        for (int counter = 0; counter < machines.size(); counter++) {

            if (machineName.equals(machines.get(counter).getName())) {
                machines.remove(counter);
                System.out.println("=========================================");
                System.out.println("Machine removed");
                System.out.println("=========================================");

            }
        }
    }
}
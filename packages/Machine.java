package machinepackge;

public class Machine {
    int id;
    String name;
    int purchaseYear;
    int engineCapacity;
    String currentStatus;

    public Machine(int id, String name, int purchaseYear, int engineCapacity, String currentStatus) {
        super();
        this.id = id;
        this.name = name;
        this.purchaseYear = purchaseYear;
        this.engineCapacity = engineCapacity;
        this.currentStatus = currentStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPurchaseYear() {
        return purchaseYear;
    }

    public void setPurchaseYear(int purchaseYear) {
        this.purchaseYear = purchaseYear;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

}
import java.util.ArrayList;
import java.util.Scanner;

import packages.*;

public class MachineManagement {

    public static void main(String[] args) {

        ArrayList<Machine> machines = new ArrayList<Machine>();
        char choice = '\0';

        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Enter your choice\n");
            System.out.println("=========================================");
            System.out.println("A: List all machines");
            System.out.println("B: Add new machine");
            System.out.println("C: Remove machine");
            System.out.println("D: Update attributes of machine");
            System.out.println("E: Search machine");
            System.out.println("F: Exit");
            System.out.println("=========================================");

            choice = sc.next().charAt(0);

            switch (choice) {
                case 'A':
                    ListMachine.listAllMachines(machines);

                    break;
                case 'B':
                    AddMachine.addMachine(machines);

                    break;
                case 'C':
                    RemoveMachine.removeMachine(machines);

                    break;
                case 'D':
                    UpdateMachine.updateMachine(machines);

                    break;
                case 'E':
                    SearchMachine.searchMachine(machines);

                    break;
                case 'F':

                    break;
                default:
                    System.out.println("Invalid choice! Please choose valid option");
                    break;
            }

        } while (choice != 'F');
    }

}
